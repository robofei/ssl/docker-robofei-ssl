[**Esse repositório foi movido para o repositório principal (SSL-Strategy) e não está mais sendo atualizado.**](https://gitlab.com/robofei/ssl/SSL-Strategy/-/tree/master/docker)


# Docker

Repositório da imagem docker do RoboFEI-SSL

## Uso

- Instalar o docker usando o script ``installDocker-Ubuntu.sh``

- Compilar o arquivo docker com o script ``dockerbuild.sh``

- Se tudo deu certo basta rodar utilizando o script ``rundocker.sh``
