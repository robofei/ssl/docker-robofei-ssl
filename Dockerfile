FROM ubuntu:20.04

# Dependencias
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    build-essential \
    cmake \
    git \
    vim \
    sudo \
    libeigen3-dev \
    python3-pip \
    python3-tk \
    protobuf-compiler \
    qt5-default \
    qtmultimedia5-dev \
    libqt5serialport5-dev \
    '^libxcb.*-dev' \
    libx11-xcb-dev \
    libglu1-mesa-dev \
    libxrender-dev \
    libxi-dev \
    libxkbcommon-dev \
    libxkbcommon-x11-dev \
    && apt-get clean

RUN pip3 install numpy Pillow sklearn twine wheel setuptools

RUN git clone https://github.com/pybind/pybind11.git && \
    cd pybind11 && mkdir build && cd build && cmake .. && make -j4 && \
    sudo make install
